#! /usr/bin/python3
# probably works for 3 only ??

import sys,time

if sys.argv[1:]: inpf = open(sys.argv[1],mode='rt',encoding='utf-8')
else: sys.stderr.write('needs input file name?\n') ; exit()


time_series_flag=False
occupancy_flag=False

hbondict={} ; countdict={}
stari=0 ; novi=0

for line in inpf:
    if line.find('(Bridge)') > 1:
        time_series_flag=True
        #outf.write(line)
    if line.find('Analysis') > -1:
        occupancy_flag=True
        time_series_flag=False
        # this is for parallel that never worked (everybody writing to a same file!!)
        # does not work for parallel yet, so just put back everything to original
        # so it reads through the whole file...
        #time_series_flag=True
        #outf.write(line)
    if time_series_flag:
        # now we are collecting data for trajectory info
        # the interesting lines are the ones with 11 elements in a split:
        # this is needed for confusing parallel output - it goes everywhere!!
        ll=line.split()
        if len(ll) == 11:
            # *CHANGE* - able - just to ignore non-relevant lines
            if line.find('PROE') > -1 :
                # construct ther key from this line:
                kluc=ll[1]+'_'+ll[3]+'-'+ll[6]+'_'+ll[8]
                vred=float(ll[9])
                if kluc in hbondict:
                    hbondict[kluc] += vred
                    countdict[kluc] += 1
                    stari += 1
                else:
                    hbondict[kluc] = vred
                    countdict[kluc]=1
                    novi += 1

print('# occurrences = ',stari, ' distinct = ',novi)
print('# dictionary length = ', len(hbondict))
# the following 4 lines are confusing ???
#najvec=max(hbondict)
#najmn=min(hbondict)
#print('maximalen cas = ', hbondict[najvec], ' za ', najvec)
#print('minimalen cas = ', hbondict[najmn], ' za ', najmn)

# order by lifetime...
from collections import OrderedDict
from operator import itemgetter
hbondict_ord=OrderedDict(sorted(hbondict.items(), key=itemgetter(1),reverse=True))
print('   H-Bond        total time[ps]    reentries')
for k,v in hbondict_ord.items(): print('{0:20s} {1:10.0f} {2:8d}'.format(k,v,countdict[k]))

intervals=140
hist=(intervals+1)*[0.0]
deltas=1000

for k, v in hbondict.items():
#    if v > 4000 : print(k,' : ', v, ' count = ', countdict[k])
    indx = int(v//deltas)
    hist[indx] += 1

outf=open('hist',mode='wt')
for i in range(intervals):
    outf.write('{0:5d}  {1:10.1f}\n'.format(i, hist[i]))


hbtyped={} ; cnttyped={} ; reentries={}
atom_types = ['HNF','NF','OF','O3','HO3','O11','O12','O13','O14']
atom_types_all =  [x for x in atom_types]
atom_types_all += ['OH2','H1','H2']
# above for lipids...
# this gets all the atom names (or types) involved in h-bonding

atom_types_all = [] ; typcount=0
for k, v in hbondict.items():
    tt=k.split('-')
    tl=tt[0].split('_')[1]
    tr=tt[1].split('_')[1]
    if typcount > 0:
        if not ( tl in atom_types_all ):
            atom_types_all.append(tl) ; typcount += 1
        if not ( tr in atom_types_all ):
            atom_types_all.append(tr) ; typcount += 1
    else:
        typcount = 1
        atom_types_all.append(tl)
        if not ( tr in atom_types_all ):
            atom_types_all.append(tr) ; typcount += 1

print()
print('These are all ', len(atom_types_all),' atom names involved in H-bonding:')
print(atom_types_all)
print()

for k, v in hbondict.items():
    tcount=0  # to process selective atom types lists
    ktl='X' ; ktr='X'
    for t in atom_types_all:
        tt=k.split('-')
#        print(k,v,t,tt)
        if tt[0].split('_')[1] == t : 
            ktl=t
            tcount += 1
        if tt[1].split('_')[1] == t : 
            ktr=t
            tcount += 1
#    if (tcount == 2) and (not (ktl[0] == 'H')) :
    if tcount == 2 :
        kt=ktl+'-'+ktr
        if kt in hbtyped:
            hbtyped[kt] += v
            cnttyped[kt] += 1
            if k in countdict:
                reentries[kt] += countdict[k]
            else:
                print("problem with ",k," in countdict  ", kt) 
        else:
            hbtyped[kt] = v
            cnttyped[kt] = 1
            reentries[kt] = countdict[k]


print('# H-bond    total life time    number of  ratio   reentries    ratio')
print('# type            [ps]         residues   [ps]')         
for k,v in hbtyped.items():
    a=v/cnttyped[k]
    b=reentries[k]/cnttyped[k]
    print('{0:9s} {1:15.1f} {2:10d} {3:10.2f} {4:10d} {5:10.2f}'
          .format(k,v,cnttyped[k],a,reentries[k],b))

#print(hbtyped)
#print(cnttyped)
